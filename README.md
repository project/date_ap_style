# AP Style Date

Output Date fields as AP Style. This module aims to meet most of these
requirements.

## About AP Style

According to the AP Style book, Dates should be formatted as such:

### Dates

Always use numerical figures, without "st," "nd," "rd," or "th."

### Days of the Week

Days of the week should be capitalized and not abbreviated.

### Datelines

 * Datelines on stories should contain a city name, entirely in CAPITAL LETTERS,
followed in most cases by the name of the state, county or territory where the
city is located: "KANSAS CITY, Kan."
 * Prominent US and international cities can stand alone in datelines without a
state or country listed. Consult the AP Stylebook for these cities.

### Months

 * Capitalize the names of months in all uses.
 * When a month is used with a specific date, you may abbreviate "Jan." "Feb."
"Aug." "Sept." "Oct." "Nov." and "Dec." All remaining months may not be
abbreviated.
 * When a phrase uses only a month and a year, do not separate the year with
commas. When a phrase refers to a month, day and year, set off the year with
commas: "January 1972 was a cold month." "His birthday is Feb. 14, 1987."

### Time

When describing events that have occurred within a seven-day time period from
the writing of the story, it is acceptable to use days of the week such as
"Monday." For any period of time beyond these seven days, use a month and a
figure for dates.

 * Use numerical figures except for "noon" and "midnight."
 * Use a colon to separate hours from minutes and keep times in lower case:
2:30 p.m.
 * Spell out the units of measurement in time sequences: "50 hours, 23 minutes,
14 seconds."

### Time before date and before place (think TDP).

Example: The speech will be 3 p.m. Thursday in Reineke Fine Arts Center. If the
event takes place within the week, use just the day; otherwise use dates.

## Configuration

* All the rule options can be set globally (at
/admin/config/content/date-ap-style). Configuration can be set for each field
formatter as well.

## Maintainers

* Shelane French (shelane) - https://www.drupal.org/u/shelane
* Chris Green (trackleft2) - https://www.drupal.org/u/trackleft2
* Brian Osborne (bkosborne) - https://www.drupal.org/u/bkosborne
* Joe Parsons (joegraduate) - https://www.drupal.org/u/joegraduate
* Jim Gauthier (usurper) - https://www.drupal.org/u/usurper
* lame https://www.drupal.org/u/lame
